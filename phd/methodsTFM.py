import numpy as np
import pandas as pd
import saveFiles as sF
from scipy import interp
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from nengo.utils.stdlib import Timer
from sklearn.decomposition import PCA
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import StratifiedShuffleSplit


################## PCA METHODS ##################

def PCA_tfm(data, n_components, scale=False, random_state=None):
    if scale==True:
        data = StandardScaler().fit_transform(data)        
    pca = PCA(n_components = n_components, random_state=random_state)
    X_pca = pca.fit_transform(data)    
    return X_pca, pca

def find_nearest_forPCA(array, value):
    array2 = np.asarray(array>=value)
    idx = (np.abs(array2 - value)).argmin()
    # Let's check if there's no True value. If there isn't, the index
    # will be the last of the array
    aux = np.where(array2==True)
    if np.array(aux).size==0:
        idx = len(array)-1 #we must take into account that the index for the
        # 1st value is zero, that's why I rest the unit
    return array[idx], idx

def select_PCA(pca, explained_variance):
    cumulative_variance = np.cumsum(pca.explained_variance_ratio_)
    value, n_components = find_nearest_forPCA(cumulative_variance, explained_variance)
    return n_components, value, cumulative_variance

def PCA_scaled_noscaled_components(X, n_components, variance=0.95):
    _, pca = PCA_tfm(X, n_components)
    _, pca_scaled = PCA_tfm(X,n_components, scale=True)

    n_components, _, cev = select_PCA(pca,variance)
    n_components_scaled, _, cev_scaled = select_PCA(pca_scaled, variance)

    return n_components, n_components_scaled, cev, cev_scaled

################## SUPPORT VECTOR MACHINE METHODS ##################

def training_SVC(x, y, kernel='linear', gamma=None, C=1, random_state=None):
    if kernel=='rbf':
        svm = OneVsRestClassifier(SVC(kernel=kernel, C=C, gamma=gamma, random_state=random_state))
    else:
        svm = OneVsRestClassifier(SVC(kernel=kernel, C=C, random_state=random_state))

    with Timer() as t:
        svm = svm.fit(x,y)
    return svm, t.duration

def test_SVC(svm, x, y):
    y_score = svm.decision_function(x)
    accuracy = np.mean(y_score.argmax(axis=1) == y.argmax(axis=1))
    return y_score, accuracy

################## PROCESS DATA METHOD ##################
# From Trevor Bekolay
def prep_data(data, columns, x_keys, x_label, y_label,
              relative_to=None, group_by=None, filter_by=None):
    data = data.copy()  # Make a copy, as we modify it

    # Make columns relative to other columns
    relative_to = [] if relative_to is None else relative_to
    for col, rel in zip(columns, relative_to):
        data[col] /= data[rel].mean()

    filter_by = [] if filter_by is None else filter_by

    extra_keys = []
    extra_keys.extend([key for key, val in filter_by])
    extra_keys.extend(relative_to)
    if group_by is not None:
        extra_keys.append(group_by)

    # Get the requested columns, and the one we're grouping by
    data = pd.concat([data[[c] + extra_keys] for c in columns],
                     keys=x_keys, names=[x_label], sort=True)
    # Merge all of the columns into one
    data[y_label] = np.nan
    for c in columns:
        data[y_label].fillna(data[c], inplace=True)
        del data[c]

    # Make the index (`x_label`) into a column
    data.reset_index(level=0, inplace=True)
    # Only take what we're filtering by
    for key, val in filter_by:
        data = data[data[key] == val]
    return data

################## ROC CURVE GENERATION ##################
# see: 
# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html#sklearn.metrics.roc_curve
def ROC_curve(y_test, y_score, labels):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    n_classes = len(labels)
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
    
    return fpr, tpr, roc_auc

def rbf_param_select(key, subdir, gamma_range, C_range):
    path, _ = sF.file_path(key=key, subdir=subdir)
    
    _,_,_,_, x_ncc_train, _, y_train_binarized, _ = sF.load_results(path)

    X = x_ncc_train

    # Debinarization step
    Y = y_train_binarized.dot(np.arange(y_train_binarized.shape[1]))

    # #############################################################################
    # Train classifiers
    #
    # For an initial search, a logarithmic grid with basis
    # 10 is often helpful. Using a basis of 2, a finer
    # tuning can be achieved but at a much higher cost.

    C_range = np.logspace(-2, 10, 13) if C_range is None else C_range
    gamma_range = np.logspace(-9, 3, 13) if gamma_range is None else gamma_range

    param_grid = dict(gamma=gamma_range, C=C_range)
    cv = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=20)
    grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv)
    grid.fit(X, Y)

    print("The best parameters are %s with a score of %0.2f"
        % (grid.best_params_, grid.best_score_))

    scores = grid.cv_results_['mean_test_score'].reshape(len(C_range),
                                                     len(gamma_range))

    return scores, C_range, gamma_range                