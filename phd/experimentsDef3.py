from __future__ import print_function

import os
import sys
import scipy
import nengo
import expTFM
import warnings
import methodsTFM
import numpy as np
import pandas as pd
import nengo.utils.numpy as npext

from nengo import spa
from scipy import stats
from .timit import TIMIT
from nengo.utils.compat import range
from nengo.utils.stdlib import Timer
from scipy.interpolate import interp1d
from nengo.cache import NoDecoderCache
from .utils import derivative, rescale
from sklearn.preprocessing import label_binarize
from . import config, parallel, saveFiles

def log(msg):
    if config.log_experiments:
        print(msg)
        sys.stdout.flush()


# #####################################
# Model 1: Neural cepstral coefficients
# #####################################
def ncc(model, sample, zscore, seed, upsample=False):
    if upsample:
        fs_scale = 50000. / TIMIT.fs
        resample_len = int(sample.shape[0] * fs_scale)
        sample = lengthen(sample, resample_len)
        model.fs = 50000

    model.audio = sample
    net = model.build(nengo.Network(seed=seed))
    with net:
        pr = nengo.Probe(net.output, synapse=0.01)
    # Disable decoder cache for this model
    _model = nengo.builder.Model(dt=0.001, decoder_cache=NoDecoderCache())
    sim = nengo.Simulator(net, model=_model)
    sim.run(model.t_audio, progress_bar=False)
    model.audio = np.zeros(1)
    feat = sim.data[pr]
    if zscore:
        feat = stats.zscore(feat, axis=0)
        feat[np.isnan(feat)] = 0.  # If variance is 0, can get nans.
    return feat


def nccs(model, audio, zscore, seed, upsample):
    out = {label: [] for label in audio}

    if parallel.get_pool() is not None:  # Asynchronous / parallel version
        jobs = {label: [] for label in audio}

        for label in audio:
            for sample in audio[label]:
                jobs[label].append(parallel.get_pool().apply_async(
                    ncc, [model, sample, zscore, seed, upsample]))
        for label in jobs:
            for result in jobs[label]:
                out[label].append(result.get())
    else:  # Synchronous / serial version
        for label in audio:
            for sample in audio[label]:
                out[label].append(ncc(model, sample, zscore, seed, upsample))

    return out


def normalize(features, n_frames):
    out = {label: [] for label in features}
    for label in features:
        for sample in features[label]:
            if sample.shape[0] < n_frames:
                sample = lengthen(sample, n_frames)
            elif sample.shape[0] > n_frames:
                sample = shorten(sample, n_frames)
            # Flatten the vector so that SVM can handle it
            out[label].append(sample.reshape(-1))
    return out


def shorten(feature, n_frames):
    """Compute neighbourhood mean to shorten the feature vector."""
    assert feature.shape[0] >= n_frames
    scale = int(feature.shape[0] / n_frames)
    pad_size = int(np.ceil(float(feature.shape[0]) / scale) * scale
                   - feature.shape[0])
    feature = np.vstack([feature,
                         np.zeros((pad_size, feature.shape[1])) * np.nan])
    return scipy.nanmean(
        feature.reshape(-1, scale, feature.shape[1]), axis=1)[:n_frames]


def lengthen(feature, n_frames):
    """Use linear interpolation to lengthen the feature vector."""
    assert feature.shape[0] <= n_frames
    if feature.shape[0] == 1:
        feature = np.tile(feature, (2, 1))
    interp_x = np.linspace(0, n_frames, feature.shape[0])
    f = interp1d(interp_x, feature, axis=0, assume_sorted=True)
    return f(np.arange(n_frames))


class AuditoryFeaturesExperiment(object):
    def __init__(self, model, phones=None, words=None,
                 zscore=None, seed=None, upsample=False):
        self.model = model
        assert phones is None or words is None, "Can only set one, not both"
        self.phones = phones
        self.words = words
        self.seed = np.random.randint(npext.maxint) if seed is None else seed
        self.zscore = zscore
        self.upsample = upsample
        self.timit = TIMIT(config.timit_root)

    def _get_audio(self, corpus):
        if self.phones is not None:
            return self.timit.phn_samples(self.phones, corpus=corpus)
        elif self.words is not None:
            return self.timit.word_samples(self.words, corpus=corpus)

    def _get_feature(self, feature, audio, n_frames=None):
        labels = sorted(list(audio))
        with Timer() as t:
            # Default to not zscoring for NCCs
            zscore = False if self.zscore is None else self.zscore
            x = nccs(self.model, audio, zscore, self.seed, self.upsample)

        log("%ss generated in %.3f seconds" % (feature.upper(), t.duration))
        if n_frames is None:
            n_frames = max(max(xx.shape[0] for xx in x[l]) for l in audio)
        x = normalize(x, n_frames)
        return np.vstack([np.vstack(x[l]) for l in labels]), t.duration
        #return np.vstack([np.vstack(x[l]) for l in labels]), t.duration, x, n_frames, x2
    @staticmethod
    def _get_labels(audio):
        labels = sorted(list(audio))
        return np.array([l for l in labels for _ in range(len(audio[l]))])

    def features_train(self, n_frames=None):
        audio_train = self._get_audio(corpus="train")
        x_ncc_train, ncc_train_time = self._get_feature('ncc', \
            audio_train, n_frames)
        y_train = self._get_labels(audio_train)
        y_train_binarized = label_binarize(y_train, classes=self.phones)
        return x_ncc_train, y_train_binarized, ncc_train_time

    def features_test(self, x_ncc_train):
        audio_test = self._get_audio(corpus="test")
        n_frames = int(x_ncc_train.shape[1] // self.model.dimensions)
        x_ncc_test, ncc_test_time = self._get_feature('ncc', audio_test, n_frames)
        y_test = self._get_labels(audio_test)
        y_test_binarized = label_binarize(y_test, classes=self.phones)
        return x_ncc_test, y_test_binarized, ncc_test_time

    def features_extraction(self, keyfile=None, subdir='ncc'):
        if keyfile is None:
            _, keyfile = saveFiles.file_path(keyfile, ext='h5', subdir=subdir)
        if saveFiles.file_exists(keyfile, ext='h5', subdir=subdir):
            log("%s.h5 already exists in the %s subfolder." %(keyfile,subdir))
        else:
            results, path = saveFiles.create_file(keyfile, ext='h5', \
                subdir=subdir)
            log("%s.h5 not in the cache. Running." % keyfile)

            # TRAINING FEATURES EXTRACTION
            log("==== Training features extraction ====")
            x_ncc_train, y_train_binarized, \
                ncc_train_time = self.features_train()

            # TESTING FEATURES EXTRACTION
            log("==== Testing features extraction ====")
            x_ncc_test, y_test_binarized, \
                ncc_test_time = self.features_test(x_ncc_train)

            log("Storing results in the %s.h5 file." %keyfile)
            data_ncc = results.create_group('data_ncc')
            data_ncc.create_dataset('train', data=x_ncc_train, compression="gzip", \
                compression_opts=9)
            data_ncc.create_dataset('test', data=x_ncc_test, compression="gzip", \
                compression_opts=9)

            y_labels = results.create_group('y_labels')
            y_labels.create_dataset('y_train_binarized', data=y_train_binarized, compression="gzip", \
                compression_opts=9)
            y_labels.create_dataset('y_test_binarized', data=y_test_binarized, compression="gzip", \
                compression_opts=9)
            results.close()

            general = pd.DataFrame({'feat_train_time': ncc_train_time, \
                'feat_test_time': ncc_test_time}, index=[0])
            general.to_pickle(os.path.join(os.path.dirname(path),"%s,general.pkl" %keyfile))

            log("Experiment %s saved to %s." %(keyfile,path))
            return path

def run_PCA_SVM(kernel='linear', C=1, gamma=None, loadfile=None, keyfile=None, \
        subdirload=None, subdir=None):

            # load the file
            log("==== Loading file ====")
            x_ncc, _, y_train_binarized, y_test_binarized, x_ncc_train, _, _, _ = saveFiles.load_results(key=loadfile,\
                subdir=subdirload)
            # PCA PERFORMANCE
            log("==== PCA performance ====")
            if x_ncc.shape[1]<=1000:
                _, ncc_pca = methodsTFM.PCA_tfm(x_ncc, x_ncc.shape[1])
            else:
                _, ncc_pca = methodsTFM.PCA_tfm(x_ncc, 1000)
                    
            cumulative_variance = np.cumsum(ncc_pca.explained_variance_ratio_)
            _, ncomp = methodsTFM.find_nearest_forPCA(cumulative_variance, 0.98)
            ncomponents_array = np.linspace(1, ncomp, ncomp, dtype=int)

            # SVM-PCA PERFORMANCE
            results, path = saveFiles.create_file(key="%s,%s"%(loadfile,keyfile), ext='h5', \
                subdir=subdir)
            log("==== SVM training and testing performance for number of components ====")
            data_PCA_svm = expTFM.PCA_svm(ncomponents_array, x_ncc, ncc_pca, x_ncc_train, \
                y_train_binarized, y_test_binarized, kernel, C, gamma, results)
            log("SVM performance completed for %s kernel with C = %d " %(kernel, C))

            # STORING DATAFRAMES
            PCA_svm = pd.DataFrame(data_PCA_svm)
        
            PCA_svm.to_pickle(os.path.join(os.path.dirname(path),"%s,%s.pkl"%(loadfile,keyfile)))

            log("Experiment %s saved to %s." %(keyfile,os.path.dirname(path)))
            return path