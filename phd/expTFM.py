import methodsTFM
import numpy as np

def PCA_svm(ncomponents_array, x_ncc, ncc_pca, x_train, y_train_binarized, \
        y_test_binarized, kernel, C, gamma, results):
        y_score = results.create_group('y_score')
        y_labels = results.create_group('y_labels')
        y_labels.create_dataset('y_train_binarized', data=y_train_binarized, compression="gzip", \
                compression_opts=9)
        y_labels.create_dataset('y_test_binarized', data=y_test_binarized, compression="gzip", \
                compression_opts=9)
        score_train = []
        score_test = []
        data = []

        for i in np.arange(len(ncomponents_array)):
            X_ncc_pca, pca = methodsTFM.PCA_tfm(x_ncc, ncomponents_array[i], scale=False,\
                random_state=0)
            cumulative_variance = np.cumsum(pca.explained_variance_ratio_)
            
            x_ncc_train_pca = X_ncc_pca[0:x_train.shape[0],:]
            x_ncc_test_pca = X_ncc_pca[x_train.shape[0]:,:]

            svm, duration = methodsTFM.training_SVC(x_ncc_train_pca, \
                y_train_binarized, kernel=kernel, C=C, gamma=gamma, random_state=20)
            y_training_score, training_acc = methodsTFM.test_SVC(svm, \
                x_ncc_train_pca, y_train_binarized)
            y_testing_score, testing_acc = methodsTFM.test_SVC(svm, \
                x_ncc_test_pca, y_test_binarized)
            
            score_train.append(y_training_score)
            score_test.append(y_testing_score)

            data.append({'explained variance': cumulative_variance[-1], 'duration': duration, 
                     'Training acc': training_acc, 'Testing acc': testing_acc, \
                         'n_PCA': ncomponents_array[i]})
                    
        y_score.create_dataset('y_score_train', data=np.asarray(score_train), compression="gzip", \
                compression_opts=9) 
        y_score.create_dataset('y_score_test', data=np.asarray(score_test), compression="gzip", \
                compression_opts=9)
        y_score.attrs["Number of components"] = "%d . First dimension" %len(ncomponents_array)
        return data