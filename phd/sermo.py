import nengo
import numpy as np
import soundfile as sf
from nengo import spa
from nengo.dists import Choice, Exponential
from nengo.networks import EnsembleArray
from nengo.utils.compat import is_array, is_string, iteritems

from . import params
from .filters import melspace
from .mfcc import mfcc
from .networks import (
    AuditoryPeriphery,
    Cepstra,
    FeedforwardDeriv,
    IntermediateDeriv
)

from .processes import ArrayProcess
from .timit import TIMIT


class ParamsObject(object):
    @property
    def net(self):
        name = self.__class__.__name__
        assert name.endswith("Params"), "Class improperly named"
        return globals()[name[:-len("Params")]]

    def kwargs(self):
        args = {}
        klass = self.__class__
        for attr in dir(klass):
            if params.is_param(getattr(klass, attr)):
                args[attr] = getattr(self, attr)
        return args

    def __setattr__(self, key, value):
        """Make sure our names are correct."""
        assert hasattr(self.__class__, key), "'%s' attr does not exist" % key
        super(ParamsObject, self).__setattr__(key, value)

    def __getstate__(self):
        """Needed for pickling."""
        # Use ParamsObject specifically in case subclass shadows it
        return ParamsObject.kwargs(self)

    def __setstate__(self, state):
        """Needed for pickling."""
        for attr, val in iteritems(state):
            setattr(self, attr, val)


# #####################################
# Model 1: Neural cepstral coefficients
# #####################################

class MFCCParams(ParamsObject):
    audio = params.NdarrayParam('audio', default=None, shape=('*', 1))
    fs = params.NumberParam('fs', default=TIMIT.fs)
    dt = params.NumberParam('dt', default=0.01)
    window_dt = params.NumberParam('window_dt', default=0.025)
    n_cepstra = params.IntParam('n_cepstra', default=13)
    n_filters = params.IntParam('n_filters', default=32)
    n_fft = params.IntParam('n_fft', default=512)
    minfreq = params.NumberParam('minfreq', default=200)
    maxfreq = params.NumberParam('maxfreq', default=8000)
    preemph = params.NumberParam('preemph', default=0)
    lift = params.NumberParam('lift', default=0)
    energy = params.BoolParam('energy', default=False)
    n_derivatives = params.IntParam('n_derivatives', default=0)
    deriv_spread = params.IntParam('deriv_spread', default=2)

    def __call__(self):
        return mfcc(**self.kwargs())


class PeripheryParams(ParamsObject):
    freqs = params.NdarrayParam(
        'freqs', default=melspace(200, 8000, 32), shape=('*',))
        # [70 to 8500] twelve
    sound_process = params.ProcessParam('sound_process', default=None)
    auditory_filter = params.StringParam(
        'auditory_filter', default='gammatone')
    rel_bandwidth = params.NumberParam('rel_bandwidth',default=1.019)
    order = params.NumberParam('order',default=3.)
    neurons_per_freq = params.IntParam('neurons_per_freq', default=8)
    fs = params.NumberParam('fs', default=TIMIT.fs)
    adaptive_neurons = params.BoolParam('adaptive_neurons', default=False)


class CepstraParams(ParamsObject):
    n_neurons = params.IntParam('n_neurons', default=20)
    n_cepstra = params.IntParam('n_cepstra', default=13)


class FeedforwardDerivParams(ParamsObject):
    n_neurons = params.IntParam('n_neurons', default=20)
    tau_fast = params.NumberParam('tau_fast', default=0.05)
    tau_slow = params.NumberParam('tau_slow', default=0.1)


class IntermediateDerivParams(ParamsObject):
    n_neurons = params.IntParam('n_neurons', default=20)
    tau = params.NumberParam('tau', default=0.1)


class AuditoryFeatures(object):
    def __init__(self):
        self.config = nengo.Config(
            nengo.Ensemble, nengo.Connection, nengo.Probe)
        self.mfcc = MFCCParams()
        self.periphery = PeripheryParams()
        self.cepstra = CepstraParams()
        self.derivatives = []
        # Set dummy audio so that pickling doesn't fail
        self.audio = np.zeros(1)

    @property
    def audio(self):
        np.testing.assert_array_equal(self.mfcc.audio,self.periphery.sound_process.array)
        return self.mfcc.audio

    @audio.setter
    def audio(self, audio_):
        if is_string(audio_):
            # Assuming this is a wav file
            audio_, fs = sf.read(audio_)
            self.fs = fs
        assert is_array(audio_)
        if audio_.ndim == 1:
            audio_ = audio_[:, np.newaxis]
        self.mfcc.audio = audio_
        self.periphery.sound_process = ArrayProcess(audio_)

    @property
    def dimensions(self):
        return self.n_cepstra * (1 + len(self.derivatives))

    @property
    def fs(self):
        assert self.mfcc.fs == self.periphery.fs
        return self.mfcc.fs

    @fs.setter
    def fs(self, fs_):
        self.mfcc.fs = fs_
        self.periphery.fs = fs_

    @property
    def freqs(self):
        assert np.allclose(self.mfcc.minfreq, self.periphery.freqs[0])
        assert np.allclose(self.mfcc.maxfreq, self.periphery.freqs[-1])
        assert self.mfcc.n_filters == self.periphery.freqs.size
        return self.periphery.freqs

    @freqs.setter
    def freqs(self, freqs_):
        self.periphery.freqs = freqs_
        self.mfcc.minfreq = freqs_[0]
        self.mfcc.maxfreq = freqs_[-1]
        self.mfcc.n_filters = freqs_.size

    @property
    def n_cepstra(self):
        assert self.mfcc.n_cepstra == self.cepstra.n_cepstra
        return self.mfcc.n_cepstra

    @n_cepstra.setter
    def n_cepstra(self, n_cepstra_):
        self.mfcc.n_cepstra = n_cepstra_
        self.cepstra.n_cepstra = n_cepstra_

    @property
    def t_audio(self):
        return self.audio.size / float(self.fs)

    def add_derivative(self, klass="IntermediateDeriv", **kwargs):
        deriv = globals()["%sParams" % klass]()
        for k, v in iteritems(kwargs):
            setattr(deriv, k, v)
        self.derivatives.append(deriv)
        self.mfcc.n_derivatives += 1
        return deriv

    def build(self, net=None):
        if net is None:
            net = nengo.Network("Sermo feature extraction")
        with net, self.config:
            self.build_periphery(net)
            self.build_cepstra(net)
            if len(self.derivatives) > 0:
                self.build_derivatives(net)
        return net

    def build_periphery(self, net):
        net.periphery = AuditoryPeriphery(**self.periphery.kwargs())

    def build_cepstra(self, net):
        net.cepstra = Cepstra(n_freqs=net.periphery.freqs.size,
                              **self.cepstra.kwargs())
        nengo.Connection(net.periphery.an.output, net.cepstra.input)
        # If no derivatives, our output is the cepstral coefficients
        net.output = net.cepstra.output

    def build_derivatives(self, net):
        net.output = nengo.Node(size_in=self.dimensions)
        nengo.Connection(net.cepstra.output, net.output[:self.n_cepstra],
                         synapse=None)

        net.derivatives = []
        target = net.cepstra  # First, do the derivative of the cepstra
        for i, deriv in enumerate(self.derivatives):
            derivnet = deriv.net(dimensions=self.n_cepstra, **deriv.kwargs())
            nengo.Connection(target.output, derivnet.input, synapse=None)
            nengo.Connection(
                derivnet.output,
                net.output[(i+1)*self.n_cepstra:(i+2)*self.n_cepstra],
                synapse=None)
            target = derivnet  # Then do derivative of derivatives
            net.derivatives.append(derivnet)
