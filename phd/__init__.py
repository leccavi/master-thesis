import warnings
# Ignore a few annoying warnings
warnings.filterwarnings("ignore", message="axes.color_cycle is deprecated.*")
warnings.filterwarnings("ignore", message="Turning off units")
warnings.filterwarnings("ignore", message=".*sparse matrix patch.*")

import brian_no_units  # Speeds things up
import brian

from . import (filters, plots, processes, sermo, timit, methodsTFM, 
               plotsTFM, saveFiles, experimentsDef3)
