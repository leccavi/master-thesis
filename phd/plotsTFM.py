import phd
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import interp
import phd.saveFiles as sF
from itertools import cycle
import methodsTFM as methods
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 14}

plt.rc('font', **font)


def cvariance_vs_ncomponents(cumulative_variance, value, n_selected_components):
    fig, (ax0, ax1) = plt.subplots(nrows=2)
    ax0.plot(cumulative_variance, '-', markersize=5)
    ax0.spines['right'].set_visible(False)
    ax0.spines['top'].set_visible(False)
    ax0.set_xlabel('Number of components')
    ax0.set_ylabel('Cumulative variance')
    ax0.set_xlim(0, cumulative_variance.shape[0])
    ax0.axhline(y=value, color = 'r', linestyle='--')

    ax1.plot(cumulative_variance, '-o', markersize=5)
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.set_xlabel('Number of components')
    ax1.set_ylabel('Cumulative variance')
    ax1.set_xlim(0, n_selected_components)
    ax1.axhline(y=value, color = 'r', linestyle='--')

    plt.subplots_adjust(hspace=1)


def plot_ROC_curve(y_test, y_score, labels, title, show_all=False):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    n_classes = len(labels)
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    # Plot all ROC curves
    fig=plt.figure()
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average (area = {0:0.2f})'
                   ''.format(roc_auc["micro"]),
             color='deeppink', linestyle=':', linewidth=4)

    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average (area = {0:0.2f})'
                   ''.format(roc_auc["macro"]),
             color='navy', linestyle=':', linewidth=4)
    
    lw = 2
    ncol=1
    pos_legend = 2
    
    if show_all==True:
        colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                     label='{0} (area = {1:0.2f})'
                     ''.format(labels[i], roc_auc[i]))
            ncol=2
            pos_legend = 2.5
        
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="center right",bbox_to_anchor=(pos_legend, 0.5), fancybox=True, shadow=False,ncol=ncol, title='Consonants Roc curves')
    plt.show()
    return fig


def print_confusion_matrix(cm, class_names, normalize=False, title='Confusion matrix',
                           figsize = (15,12), fontsize=14, cmap='Greens', annot=False, lw=.1):
    """Prints a confusion matrix, as returned by sklearn.metrics.confusion_matrix, as a heatmap.
    
    Arguments
    ---------
    confusion_matrix: numpy.ndarray
        The numpy.ndarray object returned from a call to sklearn.metrics.confusion_matrix. 
        Similarly constructed ndarrays can also be used.
    class_names: list
        An ordered list of class names, in the order they index the given confusion matrix.
    figsize: tuple
        A 2-long tuple, the first value determining the horizontal size of the ouputted figure,
        the second determining the vertical size. Defaults to (10,7).
    fontsize: int
        Font size for axes labels. Defaults to 14.
        
    Returns
    -------
    matplotlib.figure.Figure
        The resulting confusion matrix figure
    """
    fig = plt.figure(figsize=figsize)
    if normalize==True:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]          
        df_cm = pd.DataFrame(cm, index=class_names, 
                             columns=class_names)
        heatmap = sns.heatmap(df_cm, annot=annot, fmt=".2f", cmap=cmap, vmin=0, vmax=1, linewidths=lw)
    else:
        df_cm = pd.DataFrame(cm, index=class_names, 
                             columns=class_names)
        heatmap = sns.heatmap(df_cm, annot=annot, fmt="d", cmap=cmap, linewdiths=lw)
        
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=90, ha='right', fontsize=fontsize)
    plt.ylabel('True label', fontsize=fontsize)
    plt.xlabel('Predicted label', fontsize=fontsize)
    plt.title(title, fontsize=fontsize)
    np.set_printoptions(precision=2)
    return fig


# FINISH PLOT_CONF_MAT BY INTRODUCING THE OPTION OF SELECT THE VARIANCE
def plot_conf_mat(key, subdir, title=None, lw=0, fontsize=30):
    class_names = np.asarray(phd.timit.TIMIT.consonants)
    path, _ = sF.file_path(key=key, subdir=subdir); data = sF.read_file(path)

    Y_test_binarized = sF.load_dataset(data,'y_labels/y_test_binarized')
    y_test_score = sF.load_dataset(data,'y_score/y_score_test')

    cm = confusion_matrix(Y_test_binarized.argmax(axis=1),y_test_score[-1].argmax(axis=1))
    fig = print_confusion_matrix(cm, class_names=class_names, normalize=True, title=title, lw=lw, fontsize=fpntsize)
    return fig


def plot_ROCurves(binarized, scores, labels, title=None):
    n = scores.shape[0]
    area_micro = np.zeros(n)
    area_macro = np.zeros(n)
    
    fig = plt.figure()
    Z = [[0,0],[0,0]]
    levels = np.linspace(1, n, n, \
                        dtype=int)
    CS3 = plt.contourf(Z, levels, cmap=plt.cm.Blues)
    plt.clf()

    clrs = plt.cm.Blues(np.linspace(0,1,n))

    for i in range(n):
        fpr, tpr, roc_auc = methods.ROC_curve\
                            (binarized, scores[i], \
                            labels)
        
        plt.plot(fpr["macro"], tpr["macro"], color=clrs[i],
                label = '%d' %(i+1))
        area_micro[i] = roc_auc["micro"]
        area_macro[i] = roc_auc["macro"]
        
    plt.plot([0, 1], [0, 1], 'k--')

    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.xlim(0,1)
    plt.ylim(0,1)
    cbar = plt.colorbar(CS3)
    cbar.ax.set_title('PCs')
    plt.title(title)
    plt.show()

    return fig, area_macro, area_micro


 # PRINT THE HEATMAP OF SVM-RBF PARAMETER SELECTION
class MidpointNormalize(Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

def print_RBF_selection(scores, colormap, vmin, midpoint, gamma_range, C_range):
    fig = plt.figure(figsize=(8, 6))
    plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
    plt.imshow(scores, interpolation='nearest', cmap='colormap',
            norm=MidpointNormalize(vmin=vmin, midpoint=midpoint))
    plt.xlabel('gamma')
    plt.ylabel('C')
    plt.colorbar()
    plt.xticks(np.arange(len(gamma_range)), gamma_range, rotation=45)
    plt.yticks(np.arange(len(C_range)), C_range)
    plt.title('Validation accuracy')
    plt.show()
    return fig


def plot_acc_ncomp(path, labels, xlimupper=None, clrs=None, legtitle=None):
    _, train_acc, test_acc, _, _, ncomponents = sF.read_values(path)
    acc = [train_acc, test_acc]

    phd.plots.setup(figsize=(15, 13))

    sns.set_style({"axes.spines.right": False})
    sns.set_style({"axes.spines.top": False})
    sns.set_context("paper",font_scale=1.8, rc={"lines.linewidth": 3})
    sns.despine()

    clrs = ['dodgerblue','limegreen', 'orange', 'black', 'turquoise', 'orchid', 'silver', 'navy'] if\
        clrs is None else clrs

    fig=plt.figure(); plt.subplot(221); ax1 = plt.gca()

    plt.plot(ncomponents[i],acc[0],color=clrs[0])

    if xlimupper is None:
        xlimupper = np.zeros(len(path)+1)
        for i in range(len(path)):
            xlimupper[i] = ncomponents[i][-1]
    else:
        xlimupper

    plt.xlabel('Number of components');plt.ylabel('Training accuracy')

    x_labels = ax1.get_xticks().astype(int)
    x_labels[0] = 1
    ax1.set_xticklabels(x_labels)
    ax1.set_xticks(x_labels)

    plt.xlim(1, xlimupper.max()) if type(xlimupper) is np.ndarray else plt.xlim(1, xlimupper)
    plt.ylim(0)

    plt.subplot(222);ax2 = plt.gca()

    h = [plt.plot(ncomponents[i],test_acc[i], color=clrs[i], label=labels[i])[0]\
         for i in range(len(path))]
        
    plt.xlabel('Number of components'); plt.ylabel('Testing accuracy')

    x_labels = ax2.get_xticks().astype(int)
    x_labels[0] = 1
    ax2.set_xticklabels(x_labels)
    ax2.set_xticks(x_labels)

    plt.xlim(1, xlimupper.max()) if type(xlimupper) is np.ndarray else plt.xlim(1, xlimupper)
    plt.ylim(0)
    
    if legtitle is not None:
        nlines = len(labels)
        ncols = nlines if nlines<4 else 4 

        leg1 = ax2.legend(bbox_to_anchor=(-0.10, -0.45),handles=h[:nlines//ncols*ncols], ncol=ncols, loc="lower center",
            fancybox=True, title=legtitle, shadow=False, frameon=False, prop={'size': 16})
        plt.gca().add_artist(leg1)
        leg2 = ax2.legend(handles=h[nlines//ncols*ncols:], ncol=nlines-nlines//ncols*ncols)
        leg2.remove()
        leg1._legend_box._children.append(leg2._legend_handle_box)
        leg1._legend_box.stale = True
              
    return fig

def plot_mult_acc_ncomp(path, nfilters, clrs, C_val=None, multC=None):
    df = []; train_acc = []; test_acc = []; duration = []; variance = []
    ncomp = []; labels = []

    selected=C_val if multC == 'Yes' else nfilters

    for i in np.arange(len(selected)):
        values =  C_val[i] if multC == 'Yes' else (nfilters[i],nfilters[i]) \
            if nfilters[i]<=13 else (nfilters[i], 13)
        df.append(sF.load_dataframe(path%values))
        train_acc.append(df[i]["Training acc"].values)
        test_acc.append(df[i]["Testing acc"].values)
        duration.append(df[i]["duration"].values)
        variance.append(df[i]["explained variance"].values)
        labels.append(selected[i])
        ncomp.append(df[i]["n_PCA"].values)

    phd.plots.setup(figsize=(15, 13))
    sns.set_style({"axes.spines.right": False})
    sns.set_style({"axes.spines.top": False})
    sns.set_context("paper",font_scale=1.8, rc={"lines.linewidth": 3})
    sns.despine()

    fig = plt.figure(); plt.subplot(221); ax = plt.gca()
    clrs = ['dodgerblue','limegreen', 'orange', 'black', 'turquoise', 'orchid'] if\
        clrs is None else clrs

    for i in np.arange(len(selected)):
        plt.plot(ncomp[i],train_acc[i],color=clrs[i],
                label=selected[i])
    
    plt.xlabel('Number of components'); plt.ylabel('Training accuracy')

    x_labels = ax.get_xticks().astype(int)
    x_labels[0] = 1
    ax.set_xticklabels(x_labels)
    ax.set_xticks(x_labels)

    plt.xlim(1,len(df[0]["n_PCA"]))
    #plt.ylim(0)
    lg=plt.legend(loc='best', fancybox=True,
                shadow=False, frameon=False, prop={'size': 16})

    lg.set_title("Number of filters", prop = {'size':16})


    plt.subplot(222)
    ax = plt.gca()

    for i in np.arange(len(selected)):
        plt.plot(ncomp[i],test_acc[i],color=clrs[i],
                label=selected[i])
        
    plt.xlabel('Number of components')
    plt.ylabel('Testing accuracy')

    x_labels = ax.get_xticks().astype(int)
    x_labels[0] = 1
    ax.set_xticklabels(x_labels)
    ax.set_xticks(x_labels)

    plt.xlim(1,len(df[0]["n_PCA"]))

    lg=plt.legend(loc='lower right', fancybox=True,
                shadow=False, frameon=False, prop={'size': 16})

    lg.set_title("Number of filters", prop = {'size':16})
    
    return fig