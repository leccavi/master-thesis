import os
import h5py
import hashlib
import numpy as np
import pandas as pd
import timit as timit
from datetime import datetime
os.path

root_dir = "/Volumes/LeandroLV"
file_dir = os.path.join(root_dir, "TFM/results")
if not os.path.exists(file_dir):
    os.makedirs(file_dir)

def file_path(key=None, ext='h5', subdir=None):
    dir_ = file_dir if subdir is None else os.path.join(file_dir, subdir)
    key = datetime.now().strftime('%Y-%m-%d_%H.%M.%S') if key is None else key
    return os.path.join(dir_, "%s.%s" %(key, ext)), key

def file_exists(key=None, ext='h5', subdir=None):
    path, _ = file_path(key,ext,subdir)
    return os.path.exists(path)

def create_file(key=None, ext='h5', subdir=None):
    path, _ = file_path(key, ext, subdir) 
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    if ext == 'h5':
        results = h5py.File(path, "w")
    else:
        print('Only are accepted .h5 files. Try with ext=h5.')
    return results, path

def printname(name):
    print(name)

def read_file(path):
    results = h5py.File(path, "r")
    #keyfile = os.path.basename(path)
    #print('The file %s has the next items:' %keyfile)
    #results.visit(printname)
    return results

def load_dataset(results, dataset):
    dataset = np.array(results.get(dataset))
    return dataset

def load_dataframe(path):
    dataframe = pd.read_pickle(path)    
    return dataframe

def load_results(key,subdir=None):
    path, _ = file_path(key=key,subdir=subdir)
    data = read_file(path)

    X_train = load_dataset(data, 'data_ncc/train')
    X_test = load_dataset(data, 'data_ncc/test')
    Y_train_binarized = load_dataset(data, 'y_labels/y_train_binarized')
    Y_test_binarized = load_dataset(data, 'y_labels/y_test_binarized')

    # Now I perform the inverse of binarization:
    consonants = timit.TIMIT.consonants
    aux = Y_train_binarized ==1; aux2 = Y_test_binarized ==1
    aux = np.where(aux)[1]; aux2 = np.where(aux2)[1]
    Y_train = []; Y_test = []
    for i in np.arange(len(aux)):
        Y_train.append(consonants[aux[i]])
    for i in np.arange(len(aux2)):    
        Y_test.append(consonants[aux2[i]])
    Y_train = np.asarray(Y_train); Y_test = np.asarray(Y_test) 

    X=np.concatenate((X_train, X_test), axis=0)
    Y=np.concatenate((Y_train, Y_test), axis=0)

    print("X_train size: ", (X_train.shape));print("Y_train size: ", (Y_train.shape))
    print("Y_test size: ", (X_test.shape));print("X_test size: ", (Y_test.shape))
    print("Full dataset: ", (X.shape));print("Full labels: ", (Y.shape))

    return X, Y, Y_train_binarized, Y_test_binarized, X_train, X_test, Y_train, Y_test


def read_values(paths):
    df = []; train_acc = []; test_acc = []; duration = []; variance = []
    ncomp = []

    for i in range(len(paths)):
        df.append(load_dataframe(paths[i]))
        train_acc.append(df[i]["Training acc"].values)
        test_acc.append(df[i]["Testing acc"].values)
        duration.append(df[i]["duration"].values)
        variance.append(df[i]["explained variance"].values)
        ncomp.append(df[i]["n_PCA"].values)

    return df, train_acc, test_acc, duration, variance, ncomp